import 'package:calculator/app/screens/sample/vote_model.dart';
import 'package:calculator/app/screens/sample/widgets/card_element.dart';
import 'package:flutter/material.dart';

class SampleScreen extends StatelessWidget {
  SampleScreen({Key? key}) : super(key: key);

  List<Vote> myList = [
    Vote(
        title: "My long title",
        subtitle: " my subtitle my subtitle ",
        votes: 33),
    Vote(
        title: "My long title 2",
        subtitle: " my subtitle my subtitle 2 ",
        votes: 1),
    Vote(
        title: "My long title 3",
        subtitle: " my subtitle my subtitle 3 ",
        votes: 6),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.black,
        elevation: 0,
        centerTitle: false,
        title: const Text('Sample'),
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {},
        ),
        actions: [Icon(Icons.logout), Icon(Icons.person)],
      ),
      body: Column(
          children: myList
              .map((element) => Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: CardElement(
                      title: element.title,
                      subtitle: element.subtitle,
                      votes: element.votes,
                    ),
                  ))
              .toList()),
    );
  }
}
