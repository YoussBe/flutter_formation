import 'package:flutter/material.dart';

class CardElement extends StatelessWidget {
  const CardElement({
    Key? key,
    required this.title,
    required this.subtitle,
    required this.votes,
  }) : super(key: key);

  final String title;
  final String subtitle;
  final int votes;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        elevation: 5,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(title),
                        Text(subtitle),
                      ]),
                ),
              ),
              Icon(
                Icons.star,
                color: Colors.red,
              ),
              Text(votes.toString())
            ],
          ),
        ),
      ),
    );
  }
}
