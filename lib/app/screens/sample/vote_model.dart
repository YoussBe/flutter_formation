class Vote {
  Vote({required this.title, required this.subtitle, required this.votes});
  final String title;
  final String subtitle;
  final int votes;
}
