import 'package:flutter/material.dart';

class ListElement extends StatelessWidget {
  const ListElement({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(title,
                    style: const TextStyle(
                        fontSize: 30,
                        color: Colors.green,
                        fontWeight: FontWeight.bold)),
                Image.asset('assets/images/logo.png', width: 100),
              ],
            ),
          ),
          const Icon(Icons.house),
          ElevatedButton(onPressed: () {}, child: const Text("Press me!")),
        ],
      ),
    );
  }
}
